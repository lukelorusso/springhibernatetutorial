<!DOCTYPE html>
<html>
	<head>
		<title>Spring Web Customer Tracker</title>
	</head>
	<body>
		<h3>Spring Web Customer Tracker</h3>
		<ul>
			<li><a href="${pageContext.request.contextPath}/customers/list">Go to HTML Customer List</a></li>
			<li><a href="${pageContext.request.contextPath}/api/customers">Try the REST Service</a></li>
		</ul>
	</body>
</html>
