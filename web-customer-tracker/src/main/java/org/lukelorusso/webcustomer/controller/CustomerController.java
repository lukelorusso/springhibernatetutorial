package org.lukelorusso.webcustomer.controller;

import java.util.List;

import org.lukelorusso.webcustomer.entity.Customer;
import org.lukelorusso.webcustomer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/customers")
public class CustomerController {
	
	/**
	 * Injecting the Service
	 */
	@Autowired
	private CustomerService customerService;

	/**
	 * Just a redirection
	 */
	@RequestMapping(value = "/")
	public String redirect() {
		return "redirect:list";
	}

	//@RequestMapping("/list")
	@GetMapping("/list")
	public String list(Model model) {
		model.addAttribute(
			"customers",
			this.customerService.getCustomers()
		);
		return "customer-list";
	}
	
	@GetMapping("/add")
	public String add(Model model) {
		Customer c = new Customer();
		model.addAttribute("customer", c);
		return "customer-form";
	}
	
	@PostMapping("/saveCustomer")
	public String saveCustomer(@ModelAttribute("customer") Customer customer) {
		this.customerService.saveCustomer(customer);
		return "redirect:list";
	}
	
	@GetMapping("/update")
	public String update(@RequestParam("customerId") int id, Model model) {
		Customer c = this.customerService.getCustomer(id);
		model.addAttribute("customer", c);
		return "customer-form";
	}
	
	@GetMapping("/delete")
	public String delete(@RequestParam("customerId") int id, Model model) {
		this.customerService.deleteCustomer(id);
		return "redirect:list";
	}
	
	@PostMapping("/list")
    public String searchCustomers(@RequestParam("searchText") String searchText, Model model) {
		List<Customer> customerList = customerService.searchCustomers(searchText);
        model.addAttribute("customers", customerList);
        model.addAttribute("previousSearchText", searchText);
        return "customer-list";
    }
	
}
