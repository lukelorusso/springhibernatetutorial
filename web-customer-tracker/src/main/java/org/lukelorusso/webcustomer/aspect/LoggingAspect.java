package org.lukelorusso.webcustomer.aspect;

import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggingAspect { // just uncomment to see it in action!

	/*private Logger mLogger = Logger.getLogger(getClass().getName());
	
	@Pointcut("execution(* org.lukelorusso.webcustomer.controller.*.*(..))")
	private void forControllerPackage() {}
	
	@Pointcut("execution(* org.lukelorusso.webcustomer.service.*.*(..))")
	private void forServicePackage() {}
	
	@Pointcut("execution(* org.lukelorusso.webcustomer.dao.*.*(..))")
	private void forDAOPackage() {}
	
	@Pointcut("forControllerPackage() || forServicePackage() || forDAOPackage()")
	private void forAppFlow() {}
	
	@Before("forAppFlow()")
	public void before(JoinPoint jp) {
		String signature = jp.getSignature().toShortString();
		mLogger.info("==> in @Before: calling method: " + signature);
		for (Object o : jp.getArgs()) {
			mLogger.info("====> argument: " + o);
		}
	}
	
	@AfterReturning(
		pointcut = "forAppFlow()",
		returning = "result"
	)
	public void afterReturning(JoinPoint jp, Object result) {
		String signature = jp.getSignature().toShortString();
		mLogger.info(
			"==> in @AfterReturning: calling method: "
			+ signature
			+ "\n====> result: "
			+ result
		);
	}*/
}
