package org.lukelorusso.webcustomer.dao;

import java.util.List;

import org.lukelorusso.webcustomer.entity.Customer;

public interface CustomerDAO {
	
	public List<Customer> getCustomers();
	
	public Customer getCustomer(int id);

	public void saveCustomer(Customer customer);

	public void deleteCustomer(int id);

	public List<Customer> searchCustomers(String searchText);
}
