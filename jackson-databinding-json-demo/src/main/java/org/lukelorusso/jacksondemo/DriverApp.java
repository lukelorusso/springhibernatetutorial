package org.lukelorusso.jacksondemo;

import java.io.File;

import com.fasterxml.jackson.databind.ObjectMapper;

public class DriverApp {

	public static void main(String[] args) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			Student s = mapper.readValue(new File("data/sample-full.json"), Student.class);
			System.out.println(
				"First name = "
				+ s.getFirstName()
				+ "\nLast name = "
				+ s.getLastName()
				+ "\nStreet = "
				+ s.getAddress().getStreet()
				+ "\nCity = "
				+ s.getAddress().getCity()
			);
			
			for (String l : s.getLanguages()) {
				System.out.println(l);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
