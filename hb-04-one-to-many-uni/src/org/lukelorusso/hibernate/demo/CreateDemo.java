package org.lukelorusso.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.lukelorusso.hibernate.demo.entity.Course;
import org.lukelorusso.hibernate.demo.entity.Instructor;
import org.lukelorusso.hibernate.demo.entity.InstructorDetail;
import org.lukelorusso.hibernate.demo.entity.Student;

public class CreateDemo {

	public static void main(String[] args) {
		// create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.buildSessionFactory();

		Session session = null;
		try {
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			Instructor instructor = new Instructor("Chad", "Darby", "darby@lukelorusso.org");
			InstructorDetail detail = new InstructorDetail(
				"https://www.youtube.com/user/filewalkerstube",
				"developing"
			);
			instructor.setInstructorDetail(detail);
			
			session.save(instructor); // because of CascadeType.ALL this will ALSO save the detail object!
			
			System.out.println("Saved instructor: " + instructor);
			session.getTransaction().commit();
			
		} finally {
			session.close();
			factory.close();
		}
	}

}
