package org.lukelorusso.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.lukelorusso.hibernate.demo.entity.Course;
import org.lukelorusso.hibernate.demo.entity.Instructor;
import org.lukelorusso.hibernate.demo.entity.InstructorDetail;

public class CreateInstructorWithCoursesDemo {

	public static void main(String[] args) {
		// create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.buildSessionFactory();
		
		Session session = null;
		try {
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			Instructor instructor = new Instructor("Susan", "Private", "susan@lukelorusso.org");
			InstructorDetail detail = new InstructorDetail(
				"https://www.youtube.com/user/filewalkerstube",
				"video games"
			);
			instructor.setInstructorDetail(detail);

			Course c1 = new Course("Air Guitar - the ultimate guide");
			Course c2 = new Course("The Pinball Masterclass");

			instructor.add(c1);
			instructor.add(c2);
			
			session.save(instructor); // because of CascadeType.ALL this will ALSO save the detail object!
			session.save(c1);
			session.save(c2);
			
			System.out.println("Saved instructor: " + instructor);
			for (Course c : instructor.getCourseList()) {
				System.out.println("Saved course: " + c);
			}
			
			session.getTransaction().commit();
			
		} finally {
			session.close();
			factory.close();
		}
	}

}
