package org.lukelorusso.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.lukelorusso.hibernate.demo.entity.Course;
import org.lukelorusso.hibernate.demo.entity.Instructor;
import org.lukelorusso.hibernate.demo.entity.InstructorDetail;
import org.lukelorusso.hibernate.demo.entity.Review;
import org.lukelorusso.hibernate.demo.entity.Student;

public class CreateCourseWithReviewsDemo {

	public static void main(String[] args) {
		// create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.addAnnotatedClass(Review.class)
				.buildSessionFactory();

		Session session = null;
		try {
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			Course course = new Course("Pacman - How To Score One Million Points");
			course.add(new Review("Great course!"));
			course.add(new Review("R1"));
			course.add(new Review("L2"));
			
			System.out.println("Saving the course: " + course);
			for (Review r : course.getReviewList()) {
				System.out.println("Saved review: " + r);
			}
			session.save(course);
			
			session.getTransaction().commit();
			
		} finally {
			session.close();
			factory.close();
		}
	}

}
