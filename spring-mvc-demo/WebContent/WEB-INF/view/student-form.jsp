<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
	<head>
		<title>Student Registration Form</title>
	</head>
	<body>
		<form:form action="processForm" modelAttribute="student">
			First name: <form:input path="firstName" />
			<br><br>
			Last name: <form:input path="lastName" />
			<br><br>
			<form:select path="country">
				<form:options items="${student.countryMap}" />
			</form:select>
			<br><br>
			Favorite Language:
			<br><br>
			<form:radiobuttons path="language" items="${student.languageList}" /> 
			<br><br>
			Operating Systems:
			<br><br>
			Linux <form:checkbox path="osList" value="Linux" />
			macOS <form:checkbox path="osList" value="macOS" />
			MS Windows <form:checkbox path="osList" value="MS Windows" />
			<br><br>
			<input type="submit" value="submit" />
		</form:form>
	</body>
</html>