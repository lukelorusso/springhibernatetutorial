<!DOCTYPE html>
<html>
	<body>
		<h2>Spring MVC Demo - Home Page</h2>
		<hr>
		<a href="hello/">Hello World form</a>
		<br><br>
		<a href="student/">Student form</a>
		<br><br>
		<a href="customer/">Customer form</a>
		<br><br>
		<img src="${pageContext.request.contextPath}/resources/imgs/spring-by-pivotal.png">
	</body>
</html>