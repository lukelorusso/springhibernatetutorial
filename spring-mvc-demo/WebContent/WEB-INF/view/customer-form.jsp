<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
	<head>
		<title>Customer Registration Form</title>
		<style>
			.error {color:red}
		</style>
	</head>
	<body>
		<form:form action="processForm" modelAttribute="customer">
			<i>Fill out the form. Asterisk (*) means required.</i>
			<table border="0">
				<tr>
					<td>First name:</td>
					<td><form:input path="firstName" /></td>
				</tr>
				<tr>
					<td>Last name (*):</td>
					<td>
						<form:input path="lastName" />
						<form:errors path="lastName" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td>Free passes (*):</td>
					<td>
						<form:input path="freePasses" />
						<form:errors path="freePasses" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td>Postal code (*):</td>
					<td>
						<form:input path="postalCode" />
						<form:errors path="postalCode" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td>Course code:</td>
					<td>
						<form:input path="courseCode" />
						<form:errors path="courseCode" cssClass="error" />
					</td>
				</tr>
			</table>
			<br><br>
			<input type="submit" value="submit" />
		</form:form>
	</body>
</html>