package org.lukelorusso.springmvcdemo;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class Student {
	private String firstName;
	private String lastName;
	private String country;
	private String language;
	private Map<String, String> countryMap;
	private List<String> languageList;
	private List<String> osList;
	
	public Student() {
		// populating countryMap with ISO codes
		this.countryMap = new LinkedHashMap<>();
		this.countryMap.put("BE", "Belgium");
		this.countryMap.put("FR", "France");
		this.countryMap.put("DE", "Germany");
		this.countryMap.put("IT", "Italy");
		this.countryMap.put("US", "United States");

		// populating languages
		this.languageList = new ArrayList<String>();
		this.languageList.add("Java");
		this.languageList.add("C#");
		this.languageList.add("PHP");
		this.languageList.add("Ruby");

		// populating OSs
		this.osList = new ArrayList<String>();
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName.toUpperCase();
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Map<String, String> getCountryMap() {
		return countryMap;
	}

	public void setCountryMap(Map<String, String> countryMap) {
		this.countryMap = countryMap;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public List<String> getLanguageList() {
		return languageList;
	}

	public void setLanguageList(List<String> languageList) {
		this.languageList = languageList;
	}

	public List<String> getOsList() {
		return osList;
	}

	public void setOsList(List<String> osList) {
		this.osList = osList;
	}
}
