package org.lukelorusso.springmvcdemo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/student")
public class StudentController {

	/**
	 * Just a redirection
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String redirect() {
		return "redirect:showForm";
	}

	@RequestMapping("/showForm")
	public String showForm(Model theModel) {
		Student theStudent = new Student();
		theModel.addAttribute("student", theStudent);
		return "student-form";
	}
	
	@RequestMapping("/processForm")
	public String processForm(
		@ModelAttribute("student") Student theStudent
	) {
		System.out.println("theStudent: " + theStudent.getFirstName()
		+ " " + theStudent.getLastName());
		return "student-confirmation";
	}
}
