package org.lukelorusso.springmvcdemo;

import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/customer")
public class CustomerController {

	/**
	 * Just a redirection
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String redirect() {
		return "redirect:showForm";
	}
	
	/**
	 * Adding InitBinder
	 */
	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		StringTrimmerEditor ste = new StringTrimmerEditor(true);
		dataBinder.registerCustomEditor(String.class, ste);
	}

	@RequestMapping("/showForm")
	public String showForm(Model theModel) {
		theModel.addAttribute("customer", new Customer());
		return "customer-form";
	}
	
	@RequestMapping("/processForm")
	public String processForm(
		@Valid @ModelAttribute("customer") Customer theCustomer,
		BindingResult theBindingResult
	) {
		if (theBindingResult.hasErrors()) {
			FieldError lastNameFiels = theBindingResult.getFieldError("lastName");
			if (lastNameFiels != null) { 
				System.out.println("Last name: |" + theCustomer.getLastName() + "|");
			}
			
			FieldError freePasses = theBindingResult.getFieldError("freePasses");
			if (freePasses != null) {
				System.out.println("Free passes: |" + theCustomer.getFreePasses() + "|");
			}
			
			FieldError postalCode = theBindingResult.getFieldError("postalCode");
			if (postalCode != null) {
				System.out.println("Postal code: |" + theCustomer.getPostalCode() + "|");
			}
			
			return "customer-form";
		}
		
		System.out.println("theCustomer: " + theCustomer.getFirstName()
		+ " " + theCustomer.getLastName());
		return "customer-confirmation";
	}
}
