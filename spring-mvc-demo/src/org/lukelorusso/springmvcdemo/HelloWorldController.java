package org.lukelorusso.springmvcdemo;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/hello")
public class HelloWorldController {
	
	/**
	 * Just a redirection
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String redirect() {
		return "redirect:showForm";
	}
	
	/**
	 * Show form
	 */
	@RequestMapping("/showForm")
	public String showForm() {
		return "helloworld-form";
	}
	
	/**
	 * Process the form
	 */
	@RequestMapping("/processForm")
	public String processForm() {
		return "helloworld";
	}
	
	/**
	 * Controller method to put into model the HTML form data
	 */
	@RequestMapping("/processFormV2")
	public String processForm(
		HttpServletRequest request,
		Model model
	) {
		// read params from form
		String theName = request.getParameter("studentName");
		theName = theName.toUpperCase();
				
		// add message to the model
		String result = "Yo! " + theName;
		model.addAttribute("message", result);
		
		return "helloworld";
	}
	
	/**
	 * Controller method to put into model the HTML form data
	 */
	@RequestMapping("/processFormV3")
	public String processForm(
		@RequestParam("studentName") String theName,
		Model model
	) {
		// read params from form
		theName = theName.toUpperCase();
				
		// add message to the model
		String result = "Hey my friend " + theName;
		model.addAttribute("message", result);
		
		return "helloworld";
	}
}
