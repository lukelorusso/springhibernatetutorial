package org.lukelorusso.springmvcdemo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
	
	@RequestMapping("/")
	public String showPage() {
		return "home-page";
	}

	/**
	 * Hello redirection
	 */
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String redirectHello() {
		return "redirect:hello/";
	}

	/**
	 * Student redirection
	 */
	@RequestMapping(value = "/student", method = RequestMethod.GET)
	public String redirectStudent() {
		return "redirect:student/";
	}

	/**
	 * Customer redirection
	 */
	@RequestMapping(value = "/customer", method = RequestMethod.GET)
	public String redirectCustomer() {
		return "redirect:customer/";
	}
}
