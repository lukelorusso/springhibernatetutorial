package org.lukelorusso.springmvcdemo;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.lukelorusso.springmvcdemo.validation.CourseCode;

public class Customer {

	private static final String IS_REQUIRED_MSG = "is required";
	private static final int FREE_PASSES_MIN_VAL = 0;
	private static final String FREE_PASSES_MIN_MSG = "must be greater than or equal to " + FREE_PASSES_MIN_VAL;
	private static final int FREE_PASSES_MAX_VAL = 10;
	private static final String FREE_PASSES_MAX_MSG = "must be less than or equal to " + FREE_PASSES_MAX_VAL;
	private static final String POSTAL_CODE_REGEXP = "^[a-zA-Z0-9]{5}";
	private static final String POSTAL_CODE_MSG = "only 5 chars/digits";
	private static final String CUSTOM_COURSECODE_VALUE = "TOP";
	
	private String firstName;
		
	@NotNull(message = IS_REQUIRED_MSG)
	@Size(min = 1, message = IS_REQUIRED_MSG)
	private String lastName;
	
	@NotNull(message = IS_REQUIRED_MSG)
	@Min(value = FREE_PASSES_MIN_VAL, message = FREE_PASSES_MIN_MSG)
	@Max(value = FREE_PASSES_MAX_VAL, message = FREE_PASSES_MAX_MSG)
	private Integer freePasses;
	
	@NotNull(message = IS_REQUIRED_MSG)
	@Pattern(regexp = POSTAL_CODE_REGEXP, message = POSTAL_CODE_MSG)
	private String postalCode;
	
	@CourseCode(value = CUSTOM_COURSECODE_VALUE, message = "must start with " + CUSTOM_COURSECODE_VALUE)
	private String courseCode;
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName != null
			? lastName.toUpperCase()
			: null;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getFreePasses() {
		return freePasses;
	}

	public void setFreePasses(Integer freePasses) {
		this.freePasses = freePasses;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}
}
