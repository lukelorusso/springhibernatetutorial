package org.lukelorusso.springmvcdemo.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = CourseCodeConstraintValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CourseCode {
	static final String DEFAULT_VALUE = "LUV";
	
	public String value() default DEFAULT_VALUE;
	public String message() default "must start with " + DEFAULT_VALUE;
	public Class<?>[] groups() default {};
	public Class<? extends Payload>[] payload() default {};
}
