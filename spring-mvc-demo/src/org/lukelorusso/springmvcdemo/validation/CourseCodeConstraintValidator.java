package org.lukelorusso.springmvcdemo.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CourseCodeConstraintValidator
	implements ConstraintValidator<CourseCode, String> {
	
	private String coursePrefix;
	
	@Override
	public void initialize(CourseCode theCourseCode) {
		this.coursePrefix = theCourseCode.value();
	}

	@Override
	public boolean isValid(String theCourseCode, ConstraintValidatorContext cvc) {
		return theCourseCode == null
			? true
			: theCourseCode.startsWith(this.coursePrefix);
	}
	
}
