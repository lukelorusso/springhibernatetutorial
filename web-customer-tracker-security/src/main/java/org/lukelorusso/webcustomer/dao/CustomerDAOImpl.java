package org.lukelorusso.webcustomer.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.lukelorusso.webcustomer.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerDAOImpl implements CustomerDAO {
	
	/**
	 * Injecting session factory
	 */
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * Make query to retrieve customers
	 */
	@Override
	public List<Customer> getCustomers() {
		Session s = this.sessionFactory.getCurrentSession();
		// let's sort the query by lastName
		Query<Customer> q = s.createQuery(
			"from Customer ORDER by lastName",
			Customer.class
		);
		return q.getResultList();
	}

	@Override
	public void saveCustomer(Customer customer) {
		Session s = this.sessionFactory.getCurrentSession();
		/*
		 * if (id.isEmpty)
		 *     INSERT new
		 * else
		 *     UPDATE existing
		 */
		s.saveOrUpdate(customer);
	}

	@Override
	public Customer getCustomer(int id) {
		Session s = this.sessionFactory.getCurrentSession();
		Customer c = s.get(Customer.class, id);
		return c;
	}

	@Override
	public void deleteCustomer(int id) {
		Session s = this.sessionFactory.getCurrentSession();
		Query<?> q = s.createQuery("DELETE from Customer WHERE id=:customerId");
		q.setParameter("customerId", id);
		q.executeUpdate();
	}

	@Override
	public List<Customer> searchCustomers(String searchText) {
		if (searchText == null || searchText.trim().isEmpty()) {
			return this.getCustomers();
		}
		
		Session s = sessionFactory.getCurrentSession();
        // search for firstName or lastName or email, case insensitive, always sorting by lastName
        Query<Customer> q = s.createQuery(
        	"from Customer WHERE lower(firstName) LIKE :searchText OR lower(lastName) LIKE :searchText OR lower(email) LIKE :searchText ORDER by last_name",
        	Customer.class
        );
		q.setParameter("searchText", "%" + searchText.toLowerCase() + "%");
        return q.getResultList();
	}

}
