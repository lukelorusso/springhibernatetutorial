package org.lukelorusso.webcustomer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
	
	@RequestMapping("/")
	public String showPage() {
		return "index";
	}

	/**
	 * Customer redirection
	 */
	@RequestMapping("/customers")
	public String redirectCustomer() {
		return "redirect:customers/";
	}
}
