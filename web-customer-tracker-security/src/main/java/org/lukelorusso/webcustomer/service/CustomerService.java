package org.lukelorusso.webcustomer.service;

import java.util.List;

import org.lukelorusso.webcustomer.entity.Customer;

public interface CustomerService {

	public List<Customer> getCustomers();
	
	public Customer getCustomer(int id);

	public void saveCustomer(Customer customer);

	public void deleteCustomer(int id);

	public List<Customer> searchCustomers(String searchText);
	
}
