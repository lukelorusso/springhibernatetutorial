package org.lukelorusso.webcustomer.rest.exception;

import org.lukelorusso.webcustomer.rest.response.CustomerErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomerExceptionHandler {
	
	@ExceptionHandler
	public ResponseEntity<CustomerErrorResponse> handleException(CustomerNotFoundException e) {
		HttpStatus status = HttpStatus.NOT_FOUND;
		CustomerErrorResponse error = new CustomerErrorResponse(
			status.value(),
			e.getMessage(),
			System.currentTimeMillis()
		);
		return new ResponseEntity<>(error, status);
	}
	
	@ExceptionHandler
	public ResponseEntity<CustomerErrorResponse> handleException(Exception e) { // for generic error
		HttpStatus status = HttpStatus.BAD_REQUEST;
		CustomerErrorResponse error = new CustomerErrorResponse(
			status.value(),
			//e.getMessage(),
			"Something wrong just happened... please verify your input data!",
			System.currentTimeMillis()
		);
		return new ResponseEntity<>(error, status);
	}
}
