package org.lukelorusso.webcustomer.rest;

import java.util.List;

import org.lukelorusso.webcustomer.entity.Customer;
import org.lukelorusso.webcustomer.rest.exception.CustomerNotFoundException;
import org.lukelorusso.webcustomer.rest.response.CustomerSuccessResponse;
import org.lukelorusso.webcustomer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CustomerRestController {
	
	/**
	 * Injecting the Service
	 */
	@Autowired
	private CustomerService customerService;
	
	@GetMapping("/customers")
	public List<Customer> getCustomers() {
		return this.customerService.getCustomers();
	}
	
	@GetMapping("/customers/{id}")
	public Customer getCustomer(@PathVariable int id) {
		Customer c = this.customerService.getCustomer(id);
		
		if (c == null) {
			throw new CustomerNotFoundException("Customer id not found: " + id);
		}
		
		return c;
	}
	
	@PostMapping("/customers")
	public Customer addCustomer(@RequestBody Customer c) {
		c.setId(0); // id = 0 means INSERT a new, not-existing, customer
		this.customerService.saveCustomer(c);
		return c;
	}
	
	@PutMapping("/customers")
	public Customer updateCustomer(@RequestBody Customer c) {
		this.customerService.saveCustomer(c); // no need to touch anything
		return c;
	}
	
	@DeleteMapping("/customers/{id}")
	public CustomerSuccessResponse deleteCustomer(@PathVariable int id) {
		Customer c = this.customerService.getCustomer(id);
		
		if (c == null) {
			throw new CustomerNotFoundException("Customer id not found: " + id);
		}
		
		this.customerService.deleteCustomer(id);
		
		HttpStatus status = HttpStatus.OK;
		return new CustomerSuccessResponse(
			status.value(),
			"Successful deleted: " + c,
			System.currentTimeMillis()
		);
	}
}
