package org.lukelorusso.aopdemo.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.lukelorusso.aopdemo.entity.Account;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(2)
public class LoggingAspect {

	@Before("org.lukelorusso.aopdemo.aspect.AopPointcuts.forDaoPackageNotGetterNorSetter()")
	public void logging(JoinPoint jp) {
		MethodSignature signature = (MethodSignature) jp.getSignature();
		System.out.println("==> Logging stuff generically; method: " + signature);
		for (Object o : jp.getArgs()) {
			if (o instanceof Account) {
				System.out.println(
					"====> Account name: "
					+ ((Account) o).getName()
					+ ", level: "
					+ ((Account) o).getLevel()
				);
			} else {
				System.out.println("====> " + o);
			}
		}
	}
	
}
