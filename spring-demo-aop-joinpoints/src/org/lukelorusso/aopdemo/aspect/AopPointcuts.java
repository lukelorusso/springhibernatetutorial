package org.lukelorusso.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class AopPointcuts {

	@Pointcut("execution(* org.lukelorusso.aopdemo.dao.*.*(..))")
	public void forDaoPackage() {}
	
	@Pointcut("execution(* org.lukelorusso.aopdemo.dao.*.get*())")
	public void getter() {}
	
	@Pointcut("execution(* org.lukelorusso.aopdemo.dao.*.set*(..))")
	public void setter() {}
	
	@Pointcut("forDaoPackage() && !(getter() || setter())")
	public void forDaoPackageNotGetterNorSetter() {}

}
