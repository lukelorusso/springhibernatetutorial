package org.lukelorusso.springdemoone;

public interface Coach {
	public String getDailyWorkout();
	
	public String getDailyFortune();
}
