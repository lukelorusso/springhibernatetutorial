package org.lukelorusso.springdemoone;

public class BaseballCoach implements Coach {
	
	private FortuneService fortuneService;
	
	public BaseballCoach() {
		
	}
	
	public BaseballCoach(FortuneService theFortuneService) {
		this.fortuneService = theFortuneService;
	}
	
	@Override
	public String getDailyWorkout() {
		return "Do something about baseball!";
	}

	@Override
	public String getDailyFortune() {
		return this.fortuneService.getFortune();
	}
}
