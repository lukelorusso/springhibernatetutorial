package org.lukelorusso.springdemoone;

public class CricketCoach implements Coach {
	
	private FortuneService fortuneService;
	private String emailAddress, team;

	public CricketCoach() {
		System.out.println("CricketCoach: inside no-args constructor");
	}

	public void setFortuneService(FortuneService theFortuneService) {
		System.out.println("CricketCoach: inside setter method");
		this.fortuneService = theFortuneService;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	@Override
	public String getDailyWorkout() {
		return "Practice fast bowling for 15 minutes";
	}

	@Override
	public String getDailyFortune() {
		return this.fortuneService.getFortune();
	}

}
