package org.lukelorusso.springdemoone;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanScopeApp {

	public static void main(String[] args) {
		
		// Load spring config file
		ClassPathXmlApplicationContext context =
				new ClassPathXmlApplicationContext("beanScope-applicationContext.xml");
		
		// retrieve bean
		Coach theCoach = context.getBean("myCoach", Coach.class);
		Coach alphaCoach = context.getBean("myCoach", Coach.class);
		
		boolean isSameInstance = theCoach == alphaCoach;
		
		System.out.println("Is same obj? " + isSameInstance);
		System.out.println("theCoach is at: " + theCoach);
		System.out.println("alphaCoach is at: " + alphaCoach);
		
		// close context
		context.close();
	}

}
