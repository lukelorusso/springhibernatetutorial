package org.lukelorusso.aopdemo.dao;

import org.springframework.stereotype.Component;

@Component
public class MembershipDAO {

	public boolean addMembership() {
		System.out.println(getClass() + ": doing stuff -> adding a membership");
		return true;
	}
	
	public boolean gotoSleep() {
		System.out.println(getClass() + ": going to sleep...");
		return false;
	}
}
