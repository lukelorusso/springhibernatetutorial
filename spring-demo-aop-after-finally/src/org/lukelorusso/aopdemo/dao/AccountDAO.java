package org.lukelorusso.aopdemo.dao;

import java.util.ArrayList;
import java.util.List;

import org.lukelorusso.aopdemo.entity.Account;
import org.springframework.stereotype.Component;

@Component
public class AccountDAO {
	
	private String name;
	
	private String serviceCode;

	public String getName() {
		System.out.println(getClass() + ": getName()");
		return name;
	}

	public void setName(String name) {
		System.out.println(getClass() + ": setName()");
		this.name = name;
	}

	public String getServiceCode() {
		System.out.println(getClass() + ": getServiceCode()");
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		System.out.println(getClass() + ": setServiceCode()");
		this.serviceCode = serviceCode;
	}
	
	public List<Account> findAccounts(boolean tripWire) {
		// for academic purpose let's simulate an exception
		if (tripWire) {
			throw new RuntimeException("No soup for you!!!");
		}
		
		List<Account> accountList = new ArrayList<>();
		
		Account a1 = new Account("John", "Silver");
		Account a2 = new Account("Madhu", "Platinum");
		Account a3 = new Account("Luca", "Gold");

		accountList.add(a1);
		accountList.add(a2);
		accountList.add(a3);
		
		return accountList;
	}

	public void addAccount(Account account, boolean flag) {
		System.out.println(getClass() + ": doing DB work -> adding an account");
	}
	
	public boolean doWork() {
		System.out.println(getClass() + ": doing DB work -> other work");
		return false;
	}
}
