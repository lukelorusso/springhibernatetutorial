<html>

	<head>
		<title>[!] Access Denied</title>
	</head>
	
	<body>
		<h2>Access Denied</h2>
		<hr>
		
		<p>You are not authorized to access this resource...</p>
		
		<p>
			<a href="${pageContext.request.contextPath}/">Back home</a>
		</p>
		
	</body>

</html>
