<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

	<head>
		<title>Company Login Page</title>
		<style>
			.error {
				color: red;
			}
		</style>
	</head>
	
	<body>
		<h3>Company Login Page</h3>
		<form:form action="${pageContext.request.contextPath}/auth" method="POST">
			<c:if test="${param.error != null}">
				<i class="error">Sorry, invalid user/pass!</i>
			</c:if>
			<p>User name: <input type="text" name="username" /></p>
			<p>Password: <input type="password" name="password" /></p>
			<p><input type="submit" value="Login"></p>
		</form:form>
	</body>

</html>
