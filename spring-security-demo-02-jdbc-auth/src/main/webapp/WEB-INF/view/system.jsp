<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>

	<head>
		<title>Admins Page</title>
	</head>
	
	<body>
		<h2>Admins Page</h2>
		<hr>
		
		<p>
			We have our annual holiday, Caribbean cruise coming up... Register now! 
			<br>
			Keep this a secret, don't tell the regular employees LOL :)
		</p>
		
		<hr>
		
		<a href="${pageContext.request.contextPath}/">Back home</a>
		
	</body>

</html>
