package org.lukelorusso.springsecurity.config;

import java.beans.PropertyVetoException;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "org.lukelorusso.springsecurity")
@PropertySource("classpath:persistence-mysql.properties")
public class AppConfig {
	
	@Autowired
	private Environment env;
	
	private Logger logger = Logger.getLogger(getClass().getName());

	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		
		viewResolver.setPrefix("/WEB-INF/view/");
		viewResolver.setSuffix(".jsp");
		
		return viewResolver;
	}
	
	@Bean
	public DataSource securityDataSource() {
		ComboPooledDataSource securityDataSource = new ComboPooledDataSource();
		
		try {
			securityDataSource.setDriverClass(env.getProperty("jdbc.driver"));
		} catch (PropertyVetoException e) {
			throw new RuntimeException(e);
		}

		String url = env.getProperty("jdbc.url");
		String user = env.getProperty("jdbc.user");
		String pass = env.getProperty("jdbc.password");
		int initPoolSize = getIntProperty("connection.pool.initialPoolSize");
		int minPoolSize = getIntProperty("connection.pool.minPoolSize");
		int maxPoolSize = getIntProperty("connection.pool.maxPoolSize");
		int maxIdleTime = getIntProperty("connection.pool.maxIdleTime");
		
		
		securityDataSource.setJdbcUrl(url);
		logger.info(">>> jdbc.url=" + env.getProperty("jdbc.url"));
		securityDataSource.setUser(user);
		logger.info(">>> jdbc.user=" + env.getProperty("jdbc.user"));
		securityDataSource.setPassword(pass);
		securityDataSource.setInitialPoolSize(initPoolSize);
		securityDataSource.setMinPoolSize(minPoolSize);
		securityDataSource.setMaxPoolSize(maxPoolSize);
		securityDataSource.setMaxIdleTime(maxIdleTime);
		
		
		return securityDataSource;
	}
	
	private int getIntProperty(String name) {
		String val = env.getProperty(name);
		return Integer.parseInt(val);
	}
}
