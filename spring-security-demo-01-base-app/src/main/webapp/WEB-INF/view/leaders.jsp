<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>

	<head>
		<title>Leaders Page</title>
	</head>
	
	<body>
		<h2>Leaders Page</h2>
		<hr>
		
		<p>
			See you in Brazil... for our annual Leadersheep retreat!
			<br>
			Keep this a secret, don't tell the regular employees LOL :)
		</p>
		
		<hr>
		
		<a href="${pageContext.request.contextPath}/">Back home</a>
		
	</body>

</html>
