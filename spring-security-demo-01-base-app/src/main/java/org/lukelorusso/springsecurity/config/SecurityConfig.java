package org.lukelorusso.springsecurity.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	private class Roles {
		static final String EMPLOYEE = "EMPLOYEE";
		static final String MANAGER = "MANAGER";
		static final String ADMIN = "ADMIN";
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		UserBuilder users = User.withDefaultPasswordEncoder();
		auth.inMemoryAuthentication()
			.withUser(users.username("john").password("test123").roles(Roles.EMPLOYEE))
			.withUser(users.username("mary").password("test123").roles(Roles.EMPLOYEE, Roles.MANAGER))
			.withUser(users.username("susan").password("test123").roles(Roles.EMPLOYEE, Roles.ADMIN));
		
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/").hasRole(Roles.EMPLOYEE)
			.antMatchers("/leaders").hasRole(Roles.MANAGER)
			.antMatchers("/system").hasRole(Roles.ADMIN)
			.anyRequest()
			.authenticated()
			.and()
			.formLogin()
			.loginPage("/login").loginProcessingUrl("/auth").permitAll()
			.and().logout().permitAll()
			.and()
			.exceptionHandling().accessDeniedPage("/access-denied");
	}

	
}
