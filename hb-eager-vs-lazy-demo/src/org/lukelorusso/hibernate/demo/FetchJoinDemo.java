package org.lukelorusso.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.lukelorusso.hibernate.demo.entity.Course;
import org.lukelorusso.hibernate.demo.entity.Instructor;
import org.lukelorusso.hibernate.demo.entity.InstructorDetail;

public class FetchJoinDemo {

	public static void main(String[] args) {
		// create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.buildSessionFactory();
		
		Session session = null;
		try {
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			int id = 2;
			
			Query<Instructor> query = session.createQuery(
				"select i from Instructor i JOIN FETCH i.courseList WHERE i.id = :instructorId",
				Instructor.class
			);
			query.setParameter("instructorId", id);
			
			Instructor instructor = query.getSingleResult();

			System.out.println("Found instructor: " + instructor);
			for (Course c : instructor.getCourseList()) {
				System.out.println("Related course: " + c);
			}

			session.getTransaction().commit();
			
		} finally {
			session.close();
			factory.close();
		}
	}

}
