package org.lukelorusso.springsecurity.config;

import javax.sql.DataSource;

import static org.lukelorusso.springsecurity.config.RoutingTable.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private DataSource securityDataSource;
	
	public class Roles {
		public static final String EMPLOYEE = "EMPLOYEE";
		public static final String MANAGER = "MANAGER";
		public static final String ADMIN = "ADMIN";
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(securityDataSource);
		/*UserBuilder users = User.withDefaultPasswordEncoder();
		auth.inMemoryAuthentication()
			.withUser(users.username("john").password("test123").roles(Roles.EMPLOYEE))
			.withUser(users.username("mary").password("test123").roles(Roles.EMPLOYEE, Roles.MANAGER))
			.withUser(users.username("susan").password("test123").roles(Roles.EMPLOYEE, Roles.ADMIN));*/
		
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers(ROOT).hasRole(Roles.EMPLOYEE)
			.antMatchers(LEADERS).hasRole(Roles.MANAGER)
			.antMatchers(SYSTEM).hasRole(Roles.ADMIN)
			.antMatchers(REGISTRATION, REGISTRATION + ROOT, REGISTRATION + REGISTER, REGISTRATION + CONFIRMATION).permitAll()
			.anyRequest().authenticated()
			.and()
			.formLogin().loginPage("/login").loginProcessingUrl("/auth").permitAll()
			.and()
			.logout().permitAll()
			.and()
			.exceptionHandling().accessDeniedPage("/access-denied");
	}
	
	@Bean
	public UserDetailsManager userDetailsManager() {
		JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
		jdbcUserDetailsManager.setDataSource(securityDataSource);
		return jdbcUserDetailsManager;
	}

	
}
