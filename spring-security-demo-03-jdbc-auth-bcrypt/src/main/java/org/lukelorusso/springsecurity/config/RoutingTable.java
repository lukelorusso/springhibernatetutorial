package org.lukelorusso.springsecurity.config;

public class RoutingTable {
	
	private RoutingTable() {
        throw new UnsupportedOperationException();
    }
	
	public static final String ROOT = "/";
	public static final String LEADERS = "/leaders";
	public static final String SYSTEM = "/system";
	public static final String REGISTRATION = "/registration";
	public static final String REGISTER = "/register";
	public static final String CONFIRMATION = "/confirmation";
	public static final String LOGIN = "/login";
	public static final String ACCESS_DENIED = "/access-denied";

}
