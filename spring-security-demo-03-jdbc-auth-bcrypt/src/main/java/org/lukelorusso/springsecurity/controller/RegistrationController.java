package org.lukelorusso.springsecurity.controller;

import static org.lukelorusso.springsecurity.config.RoutingTable.*;
import java.util.List;

import javax.validation.Valid;

import org.lukelorusso.springsecurity.config.SecurityConfig;
import org.lukelorusso.springsecurity.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(REGISTRATION)
public class RegistrationController {
	
	@Autowired
	private UserDetailsManager mUserDetailsManager;
	
	private PasswordEncoder mPasswordEncoder = new BCryptPasswordEncoder();
	
	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		StringTrimmerEditor trimmer = new StringTrimmerEditor(true);
		dataBinder.registerCustomEditor(String.class, trimmer);
	}
	
	/**
	 * Just a redirection
	 */
	@GetMapping(ROOT)
	public String redirect() {
		return "redirect:register";
	}

	@GetMapping(REGISTER)
	public String register(Model model) {
		model.addAttribute("user", new User());
		return "register";
	}
	
	@PostMapping(CONFIRMATION)
	public String processRegistrationForm(
		@Valid @ModelAttribute("user") User user,
		BindingResult bindingResult,
		Model model
	) {
		// form validation
		if (bindingResult.hasErrors()) {
			model.addAttribute(
				"user",
				new User()
			);
			model.addAttribute(
				"registrationError",
				"User name/password can not be empty."
			);
			return "register";
		}
		
		// check the database if user already exists
		String username = user.getUsername();
		if (this.doesUserExist(username)) {
			model.addAttribute(
				"user",
				new User()
			);
			model.addAttribute(
				"registrationError",
				"This user already exists"
			);
			return "register";
		}
		
		// encrypt the password
		String encodedPassword = mPasswordEncoder.encode(user.getPassword());
		
		// prepend the encoding algorithm id
		encodedPassword = "{bcrypt}" + encodedPassword;
		
		// give user default role of "employee"
		List<GrantedAuthority> authorities =
				 AuthorityUtils.createAuthorityList(SecurityConfig.Roles.EMPLOYEE);
		
		// save user in the database
		mUserDetailsManager.createUser(
				org.springframework.security.core.userdetails.User
				.withUsername(username)
				.password(encodedPassword)
				.authorities(authorities)
				.build()
		);
		
		return "register-confirmation";
	}
	
	/**
	 * Check the database if the user already exists
	 */
	private boolean doesUserExist(String username) {
		return mUserDetailsManager.userExists(username);
	}
}
