package org.lukelorusso.springsecurity.controller;

import static org.lukelorusso.springsecurity.config.RoutingTable.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

	@GetMapping(ROOT)
	public String home() {
		return "home";
	}
	
	@GetMapping(LEADERS)
	public String leaders() {
		return "leaders";
	}
	
	@GetMapping(SYSTEM)
	public String system() {
		return "system";
	}
	
	@GetMapping(REGISTRATION)
	public String registration() {
		return "redirect:" + REGISTRATION;
	}
}
