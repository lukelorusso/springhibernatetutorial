package org.lukelorusso.springsecurity.controller;

import static org.lukelorusso.springsecurity.config.RoutingTable.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SecurityController {

	@GetMapping(LOGIN)
	public String login() {
		return "login";
	}
	
	@GetMapping(ACCESS_DENIED)
	public String accessDenied() {
		return "access-denied";
	}
}
