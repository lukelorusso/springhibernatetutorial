package org.lukelorusso.aopdemo;

import java.util.List;

import org.lukelorusso.aopdemo.dao.AccountDAO;
import org.lukelorusso.aopdemo.entity.Account;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AfterRerturningApp {

	public static void main(String[] args) {
		// reading config class
		AnnotationConfigApplicationContext c =
				new AnnotationConfigApplicationContext(DemoConfig.class);
		
		// get beans from container
		AccountDAO accountDAO = c.getBean("accountDAO", AccountDAO.class);

		List<Account> accountList = accountDAO.findAccounts(false);
		System.out.println("AfterReturningApp: " + accountList);
		
		// closing context
		c.close();
	}
}
