package org.lukelorusso.aopdemo.aspect;

import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.lukelorusso.aopdemo.entity.Account;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(2)
public class LoggingAspect {

	@Before("org.lukelorusso.aopdemo.aspect.AopPointcuts.forDaoPackageNotGetterNorSetter()")
	public void logging(JoinPoint jp) {
		MethodSignature signature = (MethodSignature) jp.getSignature();
		System.out.println("==> Logging stuff generically; method: " + signature);
		for (Object o : jp.getArgs()) {
			if (o instanceof Account) {
				System.out.println(
					"====> Account name: "
					+ ((Account) o).getName()
					+ ", level: "
					+ ((Account) o).getLevel()
				);
			} else {
				System.out.println("====> " + o);
			}
		}
	}
	
	@AfterReturning(
		pointcut = "execution(* org.lukelorusso.aopdemo.dao.AccountDAO.findAccounts(..))",
		returning = "result" // must match method argument DOWN HERE!
	)
	public void afterReturningFindAccounts(JoinPoint jp, List<Account> result) {
		String signature = jp.getSignature().toShortString();
		System.out.println(
			"==> Logging stuff generically; @AfterReturning on "
			+ signature
			+ " with result: "
			+ result
		);
		this.converAccountNamesToUpperCase(result);
	}

	private void converAccountNamesToUpperCase(List<Account> result) {
		for (Account a : result) {
			a.setName(a.getName().toUpperCase());
		}
	}
	
	@AfterThrowing(
		pointcut = "execution(* org.lukelorusso.aopdemo.dao.AccountDAO.findAccounts(..))",
		throwing = "e" // must match method argument DOWN HERE!
	)
	public void afterThrowingFindAccounts(JoinPoint jp, Throwable e) {
		String signature = jp.getSignature().toShortString();
		System.out.println(
			"==> Logging stuff generically; @AfterThrowing on "
			+ signature
			+ " with throwable: "
			+ e
		);
	}
	
}
