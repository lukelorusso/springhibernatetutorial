package org.lukelorusso.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

	@Pointcut("execution(* org.lukelorusso.aopdemo.dao.*.*(..))")
	private void forDaoPackage() {}
	
	@Pointcut("execution(* org.lukelorusso.aopdemo.dao.*.get*())")
	private void getter() {}
	
	@Pointcut("execution(* org.lukelorusso.aopdemo.dao.*.set*(..))")
	private void setter() {}
	
	@Pointcut("forDaoPackage() && !(getter() || setter())")
	private void forDaoPackageNotGetterNorSetter() {}

	@Before("forDaoPackageNotGetterNorSetter()")
	public void beforeAddAccountAdvice() {
		System.out.println("==> Executing @Before advice on addAccount()");
	}
	
	@Before("forDaoPackageNotGetterNorSetter()")
	public void performApiAnalytics() {
		System.out.println("==> Performing API analytics");
	}
	
}
