package org.lukelorusso.aopdemo.dao;

import org.lukelorusso.aopdemo.entity.Account;
import org.springframework.stereotype.Component;

@Component
public class AccountDAO {
	
	private String name;
	
	private String serviceCode;

	public String getName() {
		System.out.println(getClass() + ": getName()");
		return name;
	}

	public void setName(String name) {
		System.out.println(getClass() + ": setName()");
		this.name = name;
	}

	public String getServiceCode() {
		System.out.println(getClass() + ": getServiceCode()");
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		System.out.println(getClass() + ": setServiceCode()");
		this.serviceCode = serviceCode;
	}

	public void addAccount(Account account, boolean flag) {
		System.out.println(getClass() + ": doing DB work -> adding an account");
	}
	
	public boolean doWork() {
		System.out.println(getClass() + ": doing DB work -> other work");
		return false;
	}
}
