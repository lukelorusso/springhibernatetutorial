package org.lukelorusso.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(3)
public class ApiAnalyticsAspect {
	
	@Before("org.lukelorusso.aopdemo.aspect.AopPointcuts.forDaoPackageNotGetterNorSetter()")
	public void apiAnalytics() {
		System.out.println("==> Performing API analytics");
	}

}
