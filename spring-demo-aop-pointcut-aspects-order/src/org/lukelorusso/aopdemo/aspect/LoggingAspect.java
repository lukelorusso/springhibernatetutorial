package org.lukelorusso.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(2)
public class LoggingAspect {

	@Before("org.lukelorusso.aopdemo.aspect.AopPointcuts.forDaoPackageNotGetterNorSetter()")
	public void logging() {
		System.out.println("==> Logging stuff generically");
	}
	
}
