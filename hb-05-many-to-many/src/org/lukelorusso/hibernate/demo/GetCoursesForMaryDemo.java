package org.lukelorusso.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.lukelorusso.hibernate.demo.entity.Course;
import org.lukelorusso.hibernate.demo.entity.Instructor;
import org.lukelorusso.hibernate.demo.entity.InstructorDetail;
import org.lukelorusso.hibernate.demo.entity.Review;
import org.lukelorusso.hibernate.demo.entity.Student;

public class GetCoursesForMaryDemo {

	public static void main(String[] args) {
		// create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.addAnnotatedClass(Review.class)
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();

		Session session = null;
		try {
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			int id = 2;
			Student student = session.get(Student.class, id);
			System.out.println("Loaded student: " + student);
			for (Course c : student.getCourseList()) {
				System.out.println("Subscribed course: " + c);
			}
			
			session.getTransaction().commit();
			
			System.out.println("Done");
			
		} finally {
			session.close();
			factory.close();
		}
	}

}
