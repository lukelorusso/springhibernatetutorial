package org.lukelorusso.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.lukelorusso.hibernate.demo.entity.Course;
import org.lukelorusso.hibernate.demo.entity.Instructor;
import org.lukelorusso.hibernate.demo.entity.InstructorDetail;
import org.lukelorusso.hibernate.demo.entity.Review;
import org.lukelorusso.hibernate.demo.entity.Student;

public class CreateCourseWithStudentDemo {

	public static void main(String[] args) {
		// create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.addAnnotatedClass(Review.class)
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();

		Session session = null;
		try {
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			Course course = new Course("Pacman - How To Score One Million Points");
			
			Student s1 = new Student(
					"John",
					"Doe",
					"john@lukelorusso.org"
			);
			Student s2 = new Student(
					"Mary",
					"Public",
					"mary@lukelorusso.org"
			);
			course.add(s1);
			course.add(s2);
			
			System.out.println("Saving the course: " + course);
			session.save(course);
			
			System.out.println("Saving the students: " + course.getStudentList());
			session.save(s1);
			session.save(s2);
			
			session.getTransaction().commit();
			
			System.out.println("Done");
			
		} finally {
			session.close();
			factory.close();
		}
	}

}
