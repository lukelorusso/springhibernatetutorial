package org.lukelorusso.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class TestJdbc {

	public static void main(String[] args) {
		String jdbcUrl = "jdbc:mysql://localhost:3306/hb-05-many-to-many?useSSL=false";
		String user = "hbstudent";
		String pass = "hbstudent";
		
		System.out.println("Connecting to DB: " + jdbcUrl);
		
		try {
			Connection c = DriverManager.getConnection(jdbcUrl, user, pass);
			System.out.println("Connected!");
		} catch (Exception e) {
			System.out.println("Connection failed...");
			e.printStackTrace();
		}
	}

}
