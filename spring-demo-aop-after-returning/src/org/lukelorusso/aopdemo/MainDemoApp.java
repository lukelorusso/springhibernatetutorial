package org.lukelorusso.aopdemo;

import org.lukelorusso.aopdemo.dao.AccountDAO;
import org.lukelorusso.aopdemo.dao.MembershipDAO;
import org.lukelorusso.aopdemo.entity.Account;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainDemoApp {

	public static void main(String[] args) {
		// reading config class
		AnnotationConfigApplicationContext c =
				new AnnotationConfigApplicationContext(DemoConfig.class);
		
		// get beans from container
		AccountDAO accountDAO = c.getBean("accountDAO", AccountDAO.class);
		MembershipDAO membershipDAO = c.getBean("membershipDAO", MembershipDAO.class);
		
		// business method
		Account account = new Account();
		account.setName("Madhu");
		account.setLevel("Platinum");
		accountDAO.addAccount(account, true);
		accountDAO.doWork();
		accountDAO.setName("foobar");
		accountDAO.setServiceCode("silver");
		accountDAO.getName();
		accountDAO.getServiceCode();
				
		membershipDAO.addMembership();
		membershipDAO.gotoSleep();
		
		// closing context
		c.close();
	}
}
