package org.lukelorusso.aopdemo;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration // Spring pure Java config (no XML)
@EnableAspectJAutoProxy // Spring AOP Proxy Support
@ComponentScan("org.lukelorusso.aopdemo") // resources packages
public class DemoConfig {

}
