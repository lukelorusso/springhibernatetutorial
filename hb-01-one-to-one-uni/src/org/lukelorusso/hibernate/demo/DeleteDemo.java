package org.lukelorusso.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.lukelorusso.hibernate.demo.entity.Instructor;
import org.lukelorusso.hibernate.demo.entity.InstructorDetail;
import org.lukelorusso.hibernate.demo.entity.Student;

public class DeleteDemo {

	public static void main(String[] args) {
		// create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.buildSessionFactory();
		
		try {
			Session session = factory.getCurrentSession();
			session.beginTransaction();
			
			int id = 1;
			Instructor instructor = session.get(Instructor.class, id);
			System.out.println("Found instructor: " + instructor);
			
			if (instructor != null) {
				session.delete(instructor); // because of CascadeType.ALL this will ALSO delete the detail object!
			}
			session.getTransaction().commit();
			
			System.out.println("Instructor deleted!");
			
		} finally {
			factory.close();
		}
	}

}
