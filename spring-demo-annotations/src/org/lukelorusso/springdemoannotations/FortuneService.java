package org.lukelorusso.springdemoannotations;

public interface FortuneService {
	
	public String getFortune();
	
}
