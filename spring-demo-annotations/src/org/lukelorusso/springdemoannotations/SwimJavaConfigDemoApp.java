package org.lukelorusso.springdemoannotations;

import org.lukelorusso.springdemoannotations.Coach;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SwimJavaConfigDemoApp {

	public static void main(String[] args) {
		
		// Load spring config file
		AnnotationConfigApplicationContext context =
				new AnnotationConfigApplicationContext(SportConfig.class);
		
		// retrieve bean
		Coach theCoach = context.getBean("swimCoach", SwimCoach.class);
		
		// call methods on the bean
		System.out.println(theCoach.getDailyWorkout());
		System.out.println(theCoach.getDailyFortune());
		
		// calling swim coach methods
		System.out.println("email: " + ((SwimCoach) theCoach).getEmail());
		System.out.println("team: " + ((SwimCoach) theCoach).getTeam());
		
		// close context
		context.close();
	}

}
