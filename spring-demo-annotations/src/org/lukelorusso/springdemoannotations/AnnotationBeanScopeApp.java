package org.lukelorusso.springdemoannotations;

import org.lukelorusso.springdemoannotations.Coach;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationBeanScopeApp {

	public static void main(String[] args) {
		
		// Load spring config file
		ClassPathXmlApplicationContext context =
				new ClassPathXmlApplicationContext("applicationContext.xml");
		
		// retrieve bean
		Coach theCoach = context.getBean("tennisCoach", Coach.class);
		Coach alphaCoach = context.getBean("tennisCoach", Coach.class);
		
		boolean isSameInstance = theCoach == alphaCoach;
		
		System.out.println("Is same obj? " + isSameInstance);
		System.out.println("theCoach is at: " + theCoach);
		System.out.println("alphaCoach is at: " + alphaCoach);
		
		// close context
		context.close();
	}

}
