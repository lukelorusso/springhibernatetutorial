package org.lukelorusso.springdemoannotations;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class RandomFortuneService implements FortuneService {
	
	private String[] data = {
			"Beware of the wolf in sheep's clothing",
			"Diligence is the mother of good luck",
			"The journey is the reward"
	};

	@Override
	public String getFortune() {
		int index = new Random().nextInt(data.length);
		return data[index];
	}

}
