package org.lukelorusso.springdemoannotations;

public interface Coach {
	
	public String getDailyWorkout();
	
	public String getDailyFortune();

}
