package org.lukelorusso.springdemoannotations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class HappyFortuneService implements FortuneService {
	
	@Value("${foo.fortune}")
	private String fortune;

	@Override
	public String getFortune() {
		return fortune;
	}

}
