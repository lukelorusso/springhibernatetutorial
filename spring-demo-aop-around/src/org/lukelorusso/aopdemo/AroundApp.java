package org.lukelorusso.aopdemo;

import org.lukelorusso.aopdemo.service.TrafficFortuneService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AroundApp {

	public static void main(String[] args) {
		// reading config class
		AnnotationConfigApplicationContext c =
				new AnnotationConfigApplicationContext(DemoConfig.class);
		
		// get beans from container
		TrafficFortuneService service = c.getBean("trafficFortuneService", TrafficFortuneService.class);

		System.out.println("Calling getFortune...");
		String fortune = service.getFortune();
		System.out.println("My fortune is: " + fortune + "\nDONE!");
		
		// closing context
		c.close();
	}
}
