package org.lukelorusso.aopdemo;

import java.util.logging.Logger;

import org.lukelorusso.aopdemo.service.TrafficFortuneService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AroundWithLoggerApp {
	
	private static Logger mLogger = Logger.getLogger(AroundWithLoggerApp.class.getName());

	public static void main(String[] args) {
		// reading config class
		AnnotationConfigApplicationContext c =
				new AnnotationConfigApplicationContext(DemoConfig.class);
		
		// get beans from container
		TrafficFortuneService service = c.getBean("trafficFortuneService", TrafficFortuneService.class);

		mLogger.info("Calling getFortune...");
		String fortune = service.getFortune();
		mLogger.info("My fortune is: " + fortune + "\nDONE!");
		
		// closing context
		c.close();
	}
}
