package org.lukelorusso.aopdemo.aspect;

import java.util.List;
import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.lukelorusso.aopdemo.entity.Account;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(2)
public class LoggingAspect {
	
	private Logger mLogger = Logger.getLogger(getClass().getName());
	
	@Around("execution(* org.lukelorusso.aopdemo.service.*.getFortune(..))")
	public Object aroundGetFortune(ProceedingJoinPoint pjp) throws Throwable {
		String signature = pjp.getSignature().toShortString();
		mLogger.info(
			"==> Logging stuff generically; @Around on "
			+ signature
		);
		
		long begin = System.currentTimeMillis();
		Object result = null;
		try {
			result = pjp.proceed(); // this will actually execute the method!
		} catch (Exception e) {
			mLogger.warning(e.getMessage());
			result = "Major Accident! No worries, your private helicopter is on the way!";
			//throw e; // you may also re-throw the exception, please comment the previous line for it
		}		
		long end = System.currentTimeMillis();
		long duration = end - begin;
		
		mLogger.info(
			"==> Duration in secs: "
			+ duration / 1000.0
		);
		return result; // this will actually return the method result!
	}

	@Before("org.lukelorusso.aopdemo.aspect.AopPointcuts.forDaoPackageNotGetterNorSetter()")
	public void logging(JoinPoint jp) {
		MethodSignature signature = (MethodSignature) jp.getSignature();
		mLogger.info("==> Logging stuff generically; method: " + signature);
		for (Object o : jp.getArgs()) {
			if (o instanceof Account) {
				mLogger.info(
					"====> Account name: "
					+ ((Account) o).getName()
					+ ", level: "
					+ ((Account) o).getLevel()
				);
			} else {
				mLogger.info("====> " + o);
			}
		}
	}
	
	@After("execution(* org.lukelorusso.aopdemo.dao.AccountDAO.findAccounts(..))")
	public void afterFinallyFindAccounts(JoinPoint jp) {
		String signature = jp.getSignature().toShortString();
		mLogger.info(
			"==> Logging stuff generically; @After (finally) on "
			+ signature
		);
	}
	
	@AfterReturning(
		pointcut = "execution(* org.lukelorusso.aopdemo.dao.AccountDAO.findAccounts(..))",
		returning = "result" // must match method argument DOWN HERE!
	)
	public void afterReturningFindAccounts(JoinPoint jp, List<Account> result) {
		String signature = jp.getSignature().toShortString();
		mLogger.info(
			"==> Logging stuff generically; @AfterReturning on "
			+ signature
			+ " with result: "
			+ result
		);
		this.converAccountNamesToUpperCase(result);
	}

	private void converAccountNamesToUpperCase(List<Account> result) {
		for (Account a : result) {
			a.setName(a.getName().toUpperCase());
		}
	}
	
	@AfterThrowing(
		pointcut = "execution(* org.lukelorusso.aopdemo.dao.AccountDAO.findAccounts(..))",
		throwing = "e" // must match method argument DOWN HERE!
	)
	public void afterThrowingFindAccounts(JoinPoint jp, Throwable e) {
		String signature = jp.getSignature().toShortString();
		mLogger.info(
			"==> Logging stuff generically; @AfterThrowing on "
			+ signature
			+ " with throwable: "
			+ e
		);
	}
	
}
