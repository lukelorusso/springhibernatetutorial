package org.lukelorusso.restdemo.rest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.lukelorusso.restdemo.entity.Student;
import org.lukelorusso.restdemo.rest.exception.StudentNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class StudentRestController {
	
	private List<Student> studentList;
	
	@PostConstruct // to build the data set only once
	public void buildData() {
		this.studentList = new ArrayList<>();

		this.studentList.add(new Student(
			"Poornima",
			"Patel"
		));
		this.studentList.add(new Student(
			"Mario",
			"Rossi"
		));
		this.studentList.add(new Student(
			"Mary",
			"Smith"
		));
	}

	@GetMapping("/students")
	public List<Student> getStudents() {
		return this.studentList;
	}
	
	@GetMapping("/students/{id}")
	public Student getStudent(@PathVariable int id) {
		if (id < 0 || id >= this.studentList.size()) {
			throw new StudentNotFoundException("Student id not found: " + id);
		}
		return this.studentList.get(id);
	}
}
