package org.lukelorusso.restdemo.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class HelloRestController { // you cannot call it RestController, because of the annotation above!
	
	@GetMapping("/hello")
	public String sayHello() {
		return "Hello!";
	}
}
