package org.lukelorusso.restdemo.rest.exception;

import org.lukelorusso.restdemo.rest.response.StudentErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class StudentExceptionHandler {
	
	@ExceptionHandler
	public ResponseEntity<StudentErrorResponse> handleException(StudentNotFoundException e) {
		HttpStatus status = HttpStatus.NOT_FOUND;
		StudentErrorResponse error = new StudentErrorResponse(
			status.value(),
			e.getMessage(),
			System.currentTimeMillis()
		);
		return new ResponseEntity<>(error, status);
	}
	
	@ExceptionHandler
	public ResponseEntity<StudentErrorResponse> handleException(Exception e) { // for generic error
		HttpStatus status = HttpStatus.BAD_REQUEST;
		StudentErrorResponse error = new StudentErrorResponse(
			status.value(),
			//e.getMessage(),
			"Something wrong just happened... please verify your input data!",
			System.currentTimeMillis()
		);
		return new ResponseEntity<>(error, status);
	}
}
