package org.lukelorusso.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

	@Pointcut("execution(* org.lukelorusso.aopdemo.dao.*.*(..))")
	private void forDaoPackage() {
		
	}
	
	//@Before("execution(public void add*())")
	//@Before("execution(* add*(org.lukelorusso.aopdemo.entity.Account))") // ONLY fully qualified class names
	//@Before("execution(* add*(org.lukelorusso.aopdemo.entity.Account, ..))") // match Account param and whatevery more
	//@Before("execution(* add*(..))") // match whatevery params
	//@Before("execution(* org.lukelorusso.aopdemo.dao.*.*(..))") // every method in every class inside this package
	@Before("forDaoPackage()")
	public void beforeAddAccountAdvice() {
		System.out.println("==> Executing @Before advice on addAccount()");
	}
	
	@Before("forDaoPackage()")
	public void performApiAnalytics() {
		System.out.println("==> Performing API analytics");
	}
	
}
