package org.lukelorusso.aopdemo;

import org.lukelorusso.aopdemo.dao.AccountDAO;
import org.lukelorusso.aopdemo.dao.MembershipDAO;
import org.lukelorusso.aopdemo.entity.Account;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainDemoApp {

	public static void main(String[] args) {
		// reading config class
		AnnotationConfigApplicationContext c =
				new AnnotationConfigApplicationContext(DemoConfig.class);
		
		// get beans from container
		AccountDAO accountDAO = c.getBean("accountDAO", AccountDAO.class);
		MembershipDAO membershipDAO = c.getBean("membershipDAO", MembershipDAO.class);
		
		// business method
		accountDAO.addAccount(new Account(), true);
		accountDAO.doWork();
		membershipDAO.addMembership();
		membershipDAO.gotoSleep();
		
		// closing context
		c.close();
	}
}
