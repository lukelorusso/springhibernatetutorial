package org.lukelorusso.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.lukelorusso.hibernate.demo.entity.Student;

public class PrimaryKeyDemo {

	public static void main(String[] args) {
		// create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();
		
		// create session
		Session session = factory.getCurrentSession();
		
		try {
			// create 3 students
			System.out.println("Creating student objects...");
			Student student1 = new Student("John", "Doe", "hohn@lukelorusso.org");
			Student student2 = new Student("Mary", "Public", "mary@lukelorusso.org");
			Student student3 = new Student("Bonita", "Applebum", "bonita@lukelorusso.org");
			
			session.beginTransaction();
			System.out.println("Saving student objects...");
			session.save(student1);
			session.save(student2);
			session.save(student3);
			session.getTransaction().commit();
			System.out.println("Student objects saved!");
			
		} finally {
			factory.close();
		}
	}

}
