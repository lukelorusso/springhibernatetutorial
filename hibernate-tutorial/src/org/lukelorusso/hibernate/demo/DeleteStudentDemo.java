package org.lukelorusso.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.lukelorusso.hibernate.demo.entity.Student;

public class DeleteStudentDemo {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();
		
		try {
			Session session = factory.getCurrentSession();
			session.beginTransaction();
			
			// deleting student
			int id = 1;
			System.out.println("\nStudent object ID: " + id);
			Student student = session.get(Student.class, id);
			
			System.out.println("Deleting student...");
			System.out.println(student);
			//session.delete(student);
			session.createQuery("delete from Student WHERE id = '" + id + "'").executeUpdate();
			
			session.getTransaction().commit();
			System.out.println("Student deleted!");
			
		} finally {
			factory.close();
		}
	}

}
