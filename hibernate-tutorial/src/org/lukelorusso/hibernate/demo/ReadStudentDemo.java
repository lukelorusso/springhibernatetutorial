package org.lukelorusso.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.lukelorusso.hibernate.demo.entity.Student;

public class ReadStudentDemo {

	public static void main(String[] args) {
		// create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();
		
		// create session
		Session session = factory.getCurrentSession();
		
		try {
			// creating and saving student object
			System.out.println("Creating student object...");
			Student student = new Student("Daffy", "Duck", "daffy@lukelorusso.org");
			System.out.println("Student object created: " + student);
			
			session.beginTransaction();
			System.out.println("Saving student object...");
			session.save(student);
			session.getTransaction().commit();
			System.out.println("Student object saved!");
			
			// reading student object
			int id = student.getId();
			System.out.println("\nStudent object ID: " + id);
			
			session = factory.getCurrentSession();
			session.beginTransaction();
			Student readStudent = session.get(Student.class, id);
			System.out.println("Student object obtained: " + readStudent);
			session.getTransaction().commit();
			
		} finally {
			factory.close();
		}
	}

}
