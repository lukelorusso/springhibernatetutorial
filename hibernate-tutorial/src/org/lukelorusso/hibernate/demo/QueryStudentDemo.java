package org.lukelorusso.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.lukelorusso.hibernate.demo.entity.Student;

public class QueryStudentDemo {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();
		
		// create session
		Session session = factory.getCurrentSession();
		
		try {
			session.beginTransaction();
			
			// query students
			System.out.println("\nAll students:");
			List<Student> studentList = session
					.createQuery("from Student").getResultList();
			displayStudent(studentList);
			
			// query students by property
			System.out.println("\nStudents whose have last name of 'Doe':");
			studentList = session
					.createQuery("from Student s WHERE s.lastName = 'Doe'").getResultList();
			displayStudent(studentList);
			
			// query students by multiple properties
			System.out.println("\nStudents whose have last name of 'Doe' or first name of 'Daffy':");
			studentList = session
					.createQuery("from Student s WHERE s.lastName = 'Doe' OR s.firstName = 'Daffy'").getResultList();
			displayStudent(studentList);
			
			// query students with LIKE operator
			System.out.println("\nStudents whose have email like '%lukelorusso.org':");
			studentList = session
					.createQuery("from Student s WHERE s.email LIKE '%lukelorusso.org'").getResultList();
			displayStudent(studentList);
			
			session.getTransaction().commit();
			
		} finally {
			factory.close();
		}
	}

	private static void displayStudent(List<Student> studentList) {
		for (Student s : studentList) {
			System.out.println(s);
		}
	}

}
