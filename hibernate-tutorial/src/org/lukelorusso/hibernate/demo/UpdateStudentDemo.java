package org.lukelorusso.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.lukelorusso.hibernate.demo.entity.Student;

public class UpdateStudentDemo {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();
		
		try {
			Session session = factory.getCurrentSession();
			session.beginTransaction();
			
			// updating student's attribute
			int id = 1;
			System.out.println("\nStudent object ID: " + id);
			Student student = session.get(Student.class, id);
			
			System.out.println("Updating student...");
			student.setFirstName("Scooby");
			
			session.getTransaction().commit();
			System.out.println("Student updated!");
			
			// bulk updating for everyone
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			System.out.println("\nUpdating email for all...");
			session.createQuery("update Student SET email = 'foo@gmail.com'").executeUpdate();

			session.getTransaction().commit();
			System.out.println("Email updated!");
			
		} finally {
			factory.close();
		}
	}

}
