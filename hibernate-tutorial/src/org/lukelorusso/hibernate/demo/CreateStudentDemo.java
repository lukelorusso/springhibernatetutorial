package org.lukelorusso.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.lukelorusso.hibernate.demo.entity.Student;

public class CreateStudentDemo {

	public static void main(String[] args) {
		// create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();
		
		// create session
		Session session = factory.getCurrentSession();
		
		try {
			// use session object
			System.out.println("Creating student object...");
			Student student = new Student("Paul", "Wall", "paul@lukelorusso.org");
			
			session.beginTransaction();
			System.out.println("Saving student object...");
			session.save(student);
			session.getTransaction().commit();
			System.out.println("Student object saved!");
			
		} finally {
			factory.close();
		}
	}

}
