package org.lukelorusso.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.lukelorusso.hibernate.demo.entity.Instructor;
import org.lukelorusso.hibernate.demo.entity.InstructorDetail;
import org.lukelorusso.hibernate.demo.entity.Student;

public class GetInstructorDetailDemo {

	public static void main(String[] args) {
		// create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.buildSessionFactory();
		
		Session session = null;
		try {
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			int id = 1;
			InstructorDetail detail = session.get(InstructorDetail.class, id);
			System.out.println("Instructor detail: " + detail);
			System.out.println("The associated instructor: " + detail.getInstructor());
			
			session.getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			if (session != null) session.close();
			factory.close();
		}
	}

}
