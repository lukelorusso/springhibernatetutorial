package org.lukelorusso.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

	//@Before("execution(public void add*())")
	//@Before("execution(* add*(org.lukelorusso.aopdemo.entity.Account))") // ONLY fully qualified class names
	//@Before("execution(* add*(org.lukelorusso.aopdemo.entity.Account, ..))") // match Account param and whatevery more
	//@Before("execution(* add*(..))") // match whatevery params
	@Before("execution(* org.lukelorusso.aopdemo.dao.*.*(..))") // every method in every class inside this package
	public void beforeAddAccountAdvice() {
		System.out.println("==> Executing @Before advice on addAccount()");
	}
	
}
